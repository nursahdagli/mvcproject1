﻿using DatabaseProject2.DatabaseModelMap;
using DatabaseProject2.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DatabaseProject2.Context
{
    public class DatabaseProject2Context : DbContext
    {
        static DatabaseProject2Context()
        {
            // database migrationini engeller uygulama ayaga kalkarken database tablolarini
            // ve diger bilesenleri olusturmaya calismasini engeller
            Database.SetInitializer<DatabaseProject2Context>(null);
        }

        public DatabaseProject2Context() : base("ProjectDatabase2Connection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PersonelMapping());
            modelBuilder.Configurations.Add(new DepartmanMapping());
        }

        public DbSet<Personel> Personels { get; set; }

        public DbSet<Departman> Departmans { get; set; }


    }
}