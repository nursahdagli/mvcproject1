﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DatabaseProject2.Models
{
    public class PersonelModel
    {
        public int id { get; set; }

        [Display(Name = "Ad")]
        [Required(ErrorMessage = "Kişi Adı Gereklidir")]
        public string name { get; set; }

        [Display(Name = "Soyad")]
        [Required(ErrorMessage = "Kişi Soyadı Gereklidir")]
        public string surname { get; set; }

        [Display(Name = "Maaş")]
        [Required(ErrorMessage = "Maaş Gereklidir")]
        public int salary { get; set; }

        [Display(Name = "Age")]
        public int age { get; set; }

        [Display(Name = "Departman")]
        [Required(ErrorMessage = "Departman Gereklidir")]
        public int departmanId { get; set; }
    }
}