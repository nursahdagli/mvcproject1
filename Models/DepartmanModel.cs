﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DatabaseProject2.Models
{
    public class DepartmanModel
    {
        public int id { get; set; }

        [Display(Name="Departman Adı")]
        [Required(ErrorMessage = "Departman Adı Gereklidir")]
        public string name { get; set; }
    }
}