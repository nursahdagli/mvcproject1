﻿using DatabaseProject2.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace DatabaseProject2.DatabaseModelMap
{
    public class PersonelMapping:EntityTypeConfiguration<Personel>
    {
        public PersonelMapping()
        {
            this.ToTable("PERSONEL");
            this.HasKey(x => x.id);
            this.Property(x => x.id).HasColumnName("ID");
            this.Property(x => x.name).HasColumnName("NAME");
            this.Property(x => x.surname).HasColumnName("SURNAME");
            this.Property(x => x.salary).HasColumnName("SALARY");
            this.Property(x => x.age).HasColumnName("AGE");
            this.Property(x => x.departmanId).HasColumnName("DEPARTMAN_ID");
            this.HasRequired(x => x.Departman).WithMany(x => x.Personels).HasForeignKey(x => x.departmanId);

        }
    }
}