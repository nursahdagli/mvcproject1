﻿using DatabaseProject2.DatabaseModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace DatabaseProject2.DatabaseModelMap
{
    public class DepartmanMapping : EntityTypeConfiguration<Departman>
    {
        public DepartmanMapping()
        {
            this.ToTable("DEPARTMAN");
            this.HasKey(x => x.id);
            this.Property(x => x.id).HasColumnName("ID");
            this.Property(x => x.name).HasColumnName("NAME");
        }
    }
}