﻿using DatabaseProject2.Context;
using DatabaseProject2.DatabaseModels;
using DatabaseProject2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatabaseProject2.Controllers
{
    public class PersonelController : Controller
    {
        DatabaseProject2Context context = new DatabaseProject2Context();
        private int id;

        // GET: Personel
        public ActionResult Index()
        {
            List<Personel> personelList=context.Personels.ToList();
            ViewBag.personelList= personelList;
            return View();
        }

        public ActionResult Create()
        {
            List<Departman> departmanList = context.Departmans.ToList();
            ViewBag.departmanList = departmanList;
            return View();
        }
        [HttpPost]
        public ActionResult Create(PersonelModel model)
        {
            if (ModelState.IsValid)
            {
                Personel personel= new Personel();
                personel.age=model.age;
                personel.departmanId=model.departmanId;
                personel.name=model.name;
                personel.salary=model.salary;
                personel.surname=model.surname;
                context.Personels.Add(personel);
                context.SaveChanges();
                return RedirectToAction("Index","Personel");
            }
            return View(model);
        }
        
        public ActionResult Edit(int id)
        {
            List<Personel> personelList = context.Personels.ToList();
            List<Departman> departmanList = context.Departmans.ToList();
            ViewBag.departmanList=departmanList;

            Personel personel = personelList.Where(x=>x.id==id).FirstOrDefault();
            if (personel != null)
            {
                PersonelModel model = new PersonelModel();
                model.name = personel.name;
                model.surname = personel.surname;
                model.age = personel.age;
                model.salary = personel.salary;
                model.departmanId = personel.departmanId;
                return View(model);
            }
          
            return RedirectToAction("Index", "Personel");
        }
        [HttpPost]
        public ActionResult Edit(PersonelModel model)
        {
            List<Personel> personelList = context.Personels.ToList();
            Personel personel = personelList.Where(x => x.id == model.id).FirstOrDefault();
            if (personel != null)
            {
                if (ModelState.IsValid) { 
                    personel.age = model.age;
                    personel.departmanId = model.departmanId;
                    personel.name = model.name;
                    personel.salary = model.salary;
                    personel.surname = model.surname;
                    context.Personels.Attach(personel);
                    context.Entry(personel).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    return RedirectToAction("Index", "Personel");
                }
            }
            
            return View(model);
        }
        public ActionResult Delete(int id)
        {
            List<Personel> personelList = context.Personels.ToList();
            Personel personel = personelList.Where(x => x.id ==id).FirstOrDefault();
            if (personel != null)
            {
                context.Personels.Remove(personel);
                context.SaveChanges();
                return RedirectToAction("Index", "Personel");
            }
            return View();


        }





    }

}
