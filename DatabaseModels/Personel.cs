﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DatabaseProject2.DatabaseModels
{
    public class Personel
    {
        public int id { get; set; }

        public string name { get; set; }

        public string surname { get; set; }

        public int salary { get; set; }

        public int age { get; set; }

        public int departmanId { get; set; }

        public virtual Departman Departman { get; set; }
    }
}