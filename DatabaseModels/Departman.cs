﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DatabaseProject2.DatabaseModels
{
    public class Departman
    {
        public int id { get; set; }

        public string name { get; set; }

        public virtual List<Personel> Personels { get; set; }
    }
}